cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(openni_grabber)

find_package(PCL REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (pcdv pcdv.cpp)
target_link_libraries (pcdv ${PCL_LIBRARIES})

add_custom_command(TARGET main
                   POST_BUILD
                   COMMAND xcopy ARGS "c:\\lab\\pcl\\build\\bin\\*.dll" "$(OutDir)" "/y"
                   COMMENT "Copy dlls to the folders")