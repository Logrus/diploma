#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <iostream>
#include <boost/thread/thread.hpp>
#include <pcl/visualization/pcl_visualizer.h>
#include <vtkPropPicker.h>
// For segmentation and extraction of planars
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/region_growing.h>
// Filters
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>
// Normals
#include <pcl/features/normal_3d.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkOBJReader.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCell.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
#include <string>
#include <cstdlib>
// Timer
#include <ctime>
#include <math.h>

void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void);
void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void);
void pickPointEventOccured (const pcl::visualization::PointPickingEvent& event, 
						void* viewer_void);

pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;

int
main (int argc, char** argv)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
	viewer->setBackgroundColor (0, 0, 0);
	viewer->initCameraParameters ();
	viewer->addCoordinateSystem (1.0, "global");

	viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)&viewer);
	viewer->registerMouseCallback (mouseEventOccurred, (void*)&viewer);
	viewer->registerPointPickingCallback (pickPointEventOccured, (void*)&viewer);

	// Load file
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	if(pcl::io::loadPCDFile<pcl::PointXYZ> ("../../c1.pcd", *cloud) == -1)
		if(pcl::io::loadPCDFile<pcl::PointXYZ> ("../c1.pcd", *cloud) == -1) 
			exit(1);
	 
	ne.setInputCloud (cloud);
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
	ne.setSearchMethod (tree);
	ne.setRadiusSearch (0.03);
	ne.compute (*cloud_normals);

	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> single_color(cloud, 60, 60, 60);
	viewer->addPointCloud<pcl::PointXYZ> (cloud, single_color, "outliers");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "outliers");
	viewer->addPointCloudNormals<pcl::PointXYZ,pcl::Normal>(cloud, cloud_normals);
	
	while (!viewer->wasStopped ())
	{
		viewer->spinOnce (100);
		boost::this_thread::sleep (boost::posix_time::microseconds (100000));
	}
	return (0);
}

unsigned int text_id = 0;
void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getKeySym () == "r" && event.keyDown ())
  {
    std::cout << "r was pressed => removing all text" << std::endl;

    char str[512];
    for (unsigned int i = 0; i < text_id; ++i)
    {
      sprintf (str, "text#%03d", i);
      viewer->removeShape (str);
    }
    text_id = 0;
  }
}

void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getButton () == pcl::visualization::MouseEvent::LeftButton &&
      event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease)
  {
	  
	vtkSmartPointer<vtkPropPicker>  picker =
		vtkSmartPointer<vtkPropPicker>::New();
	picker->Pick(event.getX () , event.getY (), 0, viewer->getRendererCollection()->GetFirstRenderer());
	double* pos = picker->GetPickPosition();

	std::cout << "Pick position (world coordinates) is: "
			<< pos[0] << " " << pos[1]
			<< " " << pos[2] << std::endl;

	vtkSmartPointer<vtkActor> act (picker->GetActor());
	std::cout << "Picked actor: " << act << std::endl;
	vtkSmartPointer<vtkOBJReader> reader =
		vtkSmartPointer<vtkOBJReader>::New();
	  reader->SetFileName("socket.obj");
	  reader->Update();
	  vtkPolyData * apple = reader->GetOutput();
	  
	  //Create a sphere
      //vtkSmartPointer<vtkSphereSource> sphereSource =
      //  vtkSmartPointer<vtkSphereSource>::New();
      //sphereSource->SetCenter(pos[0], pos[1], pos[2]);
      //sphereSource->SetRadius(0.1);
 
      //Create a mapper and actor
    /*  vtkSmartPointer<vtkPolyDataMapper> mapper =
        vtkSmartPointer<vtkPolyDataMapper>::New();
      mapper->SetInput (apple);
	  
      vtkSmartPointer<vtkActor> actor =
        vtkSmartPointer<vtkActor>::New();
      actor->SetMapper(mapper);
	  actor->SetOrigin(pos[0], pos[1], pos[2]);
	  actor->SetPosition(0, 0, 0);
	  actor->SetScale(0.01);

	  viewer->getRendererCollection()->GetFirstRenderer()->AddActor(actor);*/
	
  }
}

void
pickPointEventOccured (const pcl::visualization::PointPickingEvent& event, void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getPointIndex () == -1)
    return;
  pcl::PointXYZ current_point;
  event.getPoint(current_point.x, current_point.y, current_point.z);
//  viewer->clicked_points_3d->points.push_back(current_point);
  // Draw clicked points in red:
  //pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> red (cloud, 255, 0, 0);
 //viewer->removePointCloud("clicked_points");
 //viewer->addPointCloud(cloud, red, "clicked_points");
 //viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "clicked_points");
  std::cout << current_point.x << " " << current_point.y << " " << current_point.z << std::endl;
  std::cout << "\n NORMAL: " << (cloud_normals->points[event.getPointIndex()].normal_x)*180/3.14 << " ";
  std::cout << " " << (cloud_normals->points[event.getPointIndex()].normal_y)*180/3.14 << " ";
  std::cout << " " << (cloud_normals->points[event.getPointIndex()].normal_z)*180/3.14 << " \n";
  //double normx = cloud_normals->points[event.getPointIndex()].normal_x;
  //double normy = cloud_normals->points[event.getPointIndex()].normal_y;
  //double normz = cloud_normals->points[event.getPointIndex()].normal_z;
  //pcl::computePointNormal (cloud, const std::vector<int> &indices, Eigen::Vector4f &plane_parameters, float &curvature);
  vtkSmartPointer<vtkOBJReader> reader =
		vtkSmartPointer<vtkOBJReader>::New();
	  reader->SetFileName("socket.obj");
	  reader->Update();
	  vtkPolyData * apple = reader->GetOutput();
	  
	  //Create a sphere
      //vtkSmartPointer<vtkSphereSource> sphereSource =
      //  vtkSmartPointer<vtkSphereSource>::New();
      //sphereSource->SetCenter(pos[0], pos[1], pos[2]);
      //sphereSource->SetRadius(0.1);
 
      //Create a mapper and actor
      vtkSmartPointer<vtkPolyDataMapper> mapper =
        vtkSmartPointer<vtkPolyDataMapper>::New();
      mapper->SetInput (apple);
	  
      vtkSmartPointer<vtkActor> actor =
        vtkSmartPointer<vtkActor>::New();
      actor->SetMapper(mapper);
	  
	  double orientations[3];
	  actor->GetOrientation(orientations);
	  Eigen::Vector4f normal_vector, y;
	  normal_vector = cloud_normals->points[event.getPointIndex()].getNormalVector4fMap();
	  y[0] = orientations[0]; y[1] = orientations[1]; y[2] = orientations[3]; y[3] = 1;
	  double angle = y.dot(normal_vector);
	  std::cout<<"angle: "<< angle << "\n";
	  //double nx = cloud_normals->points[event.getPointIndex()].normal_x;
	  //double ny = cloud_normals->points[event.getPointIndex()].normal_y;
	  //double nz = cloud_normals->points[event.getPointIndex()].normal_z;
	  //x = cloud_normals->points[event.getPointIndex()].getNormalVector4fMap();
	  //// double anglex = ((-1)*(cloud_normals->points[event.getPointIndex()].normal_x)*180/3.14);
	  //double anglex = ;
	  //double angley = ((-1)*(cloud_normals->points[event.getPointIndex()].normal_y)*180/3.14);
	  //double anglez = ((-1)*(cloud_normals->points[event.getPointIndex()].normal_z)*180/3.14);
	  //std::cout<<"x " << anglex << " ";
	  //std::cout<<"y " << angley << " ";
	  //std::cout<<"z " << anglez << " \n";
	  actor->SetOrientation(0,180,0);
	  actor->SetOrigin(current_point.x, current_point.y, current_point.z);
	  actor->SetPosition(0, 0, 0);
	  actor->SetScale(0.01);

	  viewer->getRendererCollection()->GetFirstRenderer()->AddActor(actor);
}