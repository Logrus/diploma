#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <iostream>
#include <boost/thread/thread.hpp>
#include <pcl/visualization/pcl_visualizer.h>
#include <vtkPropPicker.h>
// For segmentation and extraction of planars
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/region_growing.h>
// Filters
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>
// Normals
#include <pcl/features/normal_3d.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkOBJReader.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCell.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
#include <string>
#include <cstdlib>
// Timer
#include <ctime>

void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void);
void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void);

int
main (int argc, char** argv)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
	viewer->setBackgroundColor (0, 0, 0);
	viewer->initCameraParameters ();
	viewer->addCoordinateSystem (1.0, "global");

	viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)&viewer);
	viewer->registerMouseCallback (mouseEventOccurred, (void*)&viewer);


	// Load file
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	if(pcl::io::loadPCDFile<pcl::PointXYZ> ("../../c1.pcd", *cloud) == -1)
		if(pcl::io::loadPCDFile<pcl::PointXYZ> ("../c1.pcd", *cloud) == -1) 
			exit(1);
	
	// Create tree
	pcl::search::Search<pcl::PointXYZ>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZ> > (new pcl::search::KdTree<pcl::PointXYZ>);
	pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
	pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimator;
	normal_estimator.setSearchMethod (tree);
	normal_estimator.setInputCloud (cloud);
	normal_estimator.setKSearch (50);
	normal_estimator.compute (*normals);

	// Region growing part
	
	pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
	reg.setMinClusterSize (100);
	reg.setMaxClusterSize (100000000);
	reg.setSearchMethod (tree);
	reg.setNumberOfNeighbours (30);
	reg.setInputCloud (cloud);
	//reg.setIndices (indices);
	reg.setInputNormals (normals);
	reg.setSmoothnessThreshold (3.0 / 180.0 * M_PI);
	reg.setCurvatureThreshold (1.0);

	std::vector <pcl::PointIndices> clusters;
	reg.extract (clusters);
	pcl::ExtractIndices<pcl::PointXYZ> extract;
	
	pcl::PointIndices::Ptr indecies (new pcl::PointIndices ());

	extract.setInputCloud(cloud);
		pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> single_color(cloud, 60, 60, 60);
	viewer->addPointCloud<pcl::PointXYZ> (cloud, single_color, "outliers");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "outliers");
	std::cout<<"Getready!\n";
	int i=0;
	for ( int cluster_index = 0 ; cluster_index < clusters.size(); cluster_index++)
	{
		time_t tstart, tend; 
		std::cout<<"Cluster index: "<<cluster_index<<"\n";
		indecies->indices = clusters[cluster_index].indices;

		extract.setIndices(indecies);
		extract.setNegative (false);
		extract.filter(*cloud_filtered);
		
		int color1 = rand() % 255;
		int color2 = rand() % 255;
		int color3 = rand() % 255;

		pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> single_color(cloud_filtered, color1, color2, color3);
		viewer->addPointCloud<pcl::PointXYZ> (cloud_filtered, single_color, "cloud"+boost::lexical_cast<std::string>(i));
		viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "cloud"+boost::lexical_cast<std::string>(i));


		// Now we want to compute a bounding box
		Eigen::Vector4f centroid;
		pcl::compute3DCentroid(*cloud_filtered, centroid);
		Eigen::Matrix3f covariance;
		pcl::computeCovarianceMatrixNormalized(*cloud_filtered, centroid, covariance);
		Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
		Eigen::Matrix3f eigDx = eigen_solver.eigenvectors();
		eigDx.col(2) =  eigDx.col(0).cross(eigDx.col(1));
 
		Eigen::Matrix4f p2w(Eigen::Matrix4f::Identity());
		p2w.block<3,3>(0,0) = eigDx.transpose();
		p2w.block<3,1>(0,3) = -1.f * (p2w.block<3,3>(0,0) * centroid.head<3>());
		pcl::PointCloud<pcl::PointXYZ> cPoints;
		pcl::transformPointCloud(*cloud_filtered, cPoints, p2w);
 
		pcl::PointXYZ minPt, maxPt;
		pcl::getMinMax3D(cPoints, minPt, maxPt);
		const Eigen::Vector3f mean_diag = 0.5f * (maxPt.getVector3fMap() + minPt.getVector3fMap());
 
		const Eigen::Quaternionf qfinal(eigDx);
		const Eigen::Vector3f tfinal = eigDx*mean_diag + centroid.head<3>();
		
		viewer->addCube(tfinal, qfinal, maxPt.x - minPt.x, maxPt.y - minPt.y, maxPt.z - minPt.z, "cube"+boost::lexical_cast<std::string>(i));
		viewer->getRendererCollection()->GetFirstRenderer();

		/*viewer->addText ("Dimention x: " + boost::lexical_cast<std::string>(maxPt.x - minPt.x), 10, 60, "x");
		viewer->addText ("Dimention y: " + boost::lexical_cast<std::string>(maxPt.y - minPt.y), 10, 43, "y");
		viewer->addText ("Dimention z: " + boost::lexical_cast<std::string>(maxPt.z - minPt.z), 10, 30, "z");*/

		std::cout  
			 << "xmin: " << minPt.x << " " 
             << "xmax: " << maxPt.x << std::endl
             << "ymin: " << minPt.y << " " 
             << "ymax: " << maxPt.y << std::endl
             << "zmin: " << minPt.z << " " 
             << "zmax: " << maxPt.z << std::endl;

		std::cout<<"Dim x:"<<(maxPt.x - minPt.x)<<"\n";
		std::cout<<"Dim y:"<<(maxPt.y - minPt.y)<<"\n";
		std::cout<<"Dim z:"<<(maxPt.z - minPt.z)<<"\n";

		// Create the filterinstd::cout<<"Dim x:"<<maxPt.x - minPt.x<<"\n";g object
		//cloud.swap (cloud_filtered);
		indecies->indices.clear();
		i++;
		
		//viewer->spin();

		viewer->removeShape("x");
		viewer->removeShape("y");
		viewer->removeShape("z");
	}

	
	while (!viewer->wasStopped ())
	{
		viewer->spinOnce (100);
		boost::this_thread::sleep (boost::posix_time::microseconds (100000));
	}
	return (0);
}

unsigned int text_id = 0;
void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getKeySym () == "r" && event.keyDown ())
  {
    std::cout << "r was pressed => removing all text" << std::endl;

    char str[512];
    for (unsigned int i = 0; i < text_id; ++i)
    {
      sprintf (str, "text#%03d", i);
      viewer->removeShape (str);
    }
    text_id = 0;
  }
}

void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getButton () == pcl::visualization::MouseEvent::LeftButton &&
      event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease)
  {
	  
	vtkSmartPointer<vtkPropPicker>  picker =
		vtkSmartPointer<vtkPropPicker>::New();
	picker->Pick(event.getX () , event.getY (), 0, viewer->getRendererCollection()->GetFirstRenderer());
	double* pos = picker->GetPickPosition();

	std::cout << "Pick position (world coordinates) is: "
			<< pos[0] << " " << pos[1]
			<< " " << pos[2] << std::endl;

	vtkSmartPointer<vtkActor> act (picker->GetActor());
	std::cout << "Picked actor: " << act << std::endl;
	vtkSmartPointer<vtkOBJReader> reader =
		vtkSmartPointer<vtkOBJReader>::New();
	  reader->SetFileName("socket.obj");
	  reader->Update();
	  vtkPolyData * apple = reader->GetOutput();
	  
	  //Create a sphere
      //vtkSmartPointer<vtkSphereSource> sphereSource =
      //  vtkSmartPointer<vtkSphereSource>::New();
      //sphereSource->SetCenter(pos[0], pos[1], pos[2]);
      //sphereSource->SetRadius(0.1);
 
      //Create a mapper and actor
      vtkSmartPointer<vtkPolyDataMapper> mapper =
        vtkSmartPointer<vtkPolyDataMapper>::New();
      mapper->SetInput (apple);
	  
      vtkSmartPointer<vtkActor> actor =
        vtkSmartPointer<vtkActor>::New();
      actor->SetMapper(mapper);
	  actor->SetOrigin(pos[0], pos[1], pos[2]);
	  actor->SetPosition(0, 0, 0);
	  actor->SetScale(0.01);

	  viewer->getRendererCollection()->GetFirstRenderer()->AddActor(actor);
	
  }
}