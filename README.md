# The Diploma Project on "Automated measurement of three-dimensional reconstruction of objects" #

**Main screen:**
![Скриншот 2014-08-10 23.51.59.png](https://bitbucket.org/repo/j7p45G/images/879550443-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202014-08-10%2023.51.59.png)

Loaded 1 point cloud:
![Скриншот 2014-08-10 23.52.53.png](https://bitbucket.org/repo/j7p45G/images/22939384-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202014-08-10%2023.52.53.png)

Making measurments:
![Скриншот 2014-08-10 23.54.05.png](https://bitbucket.org/repo/j7p45G/images/516835570-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202014-08-10%2023.54.05.png)

Manipulating with 3D objecs in the scene:
![Скриншот 2014-08-10 23.56.19.png](https://bitbucket.org/repo/j7p45G/images/948144717-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202014-08-10%2023.56.19.png)

Creating a report:
![Скриншот 2014-08-10 23.57.45.png](https://bitbucket.org/repo/j7p45G/images/2551654537-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202014-08-10%2023.57.45.png)