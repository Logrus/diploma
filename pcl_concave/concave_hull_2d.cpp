#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/io.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
#include <iostream>
#include <string>

//#define DEBUG

int
main (int argc, char** argv)
{
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>), 
                                      cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), 
                                      cloud_projected (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PCDReader reader;
#ifdef DEBUG
  reader.read ("../table_scene_mug_stereo_textured.pcd", *cloud);
#else
  reader.read ("../../table_scene_mug_stereo_textured.pcd", *cloud);
#endif
  // Build a filter to remove spurious NaNs
  pcl::PassThrough<pcl::PointXYZ> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0, 1.1);
  pass.filter (*cloud_filtered);
  std::cerr << "PointCloud after filtering has: "
            << cloud_filtered->points.size () << " data points." << std::endl;

  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  seg.setInputCloud (cloud_filtered);
  seg.segment (*inliers, *coefficients);
  std::cerr << "PointCloud after segmentation has: "
            << inliers->indices.size () << " inliers." << std::endl;

  // Project the model inliers
  pcl::ProjectInliers<pcl::PointXYZ> proj;
  proj.setModelType (pcl::SACMODEL_PLANE);
  proj.setIndices (inliers);
  proj.setInputCloud (cloud_filtered);
  proj.setModelCoefficients (coefficients);
  proj.filter (*cloud_projected);
  std::cerr << "PointCloud after projection has: "
            << cloud_projected->points.size () << " data points." << std::endl;

  // Create a Concave Hull representation of the projected inliers
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::ConcaveHull<pcl::PointXYZ> chull;
  chull.setInputCloud (cloud_projected);
  chull.setAlpha (0.1);
  chull.reconstruct (*cloud_hull);

  std::cerr << "Concave hull has: " << cloud_hull->points.size ()
            << " data points." << std::endl;

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::visualization::PCLVisualizer viewer("PCL Viewer");
  viewer.setBackgroundColor (0.0, 0.0, 0.0);
  viewer.addPointCloud<pcl::PointXYZ>(cloud);

  cloud_filtered.swap(cloud_hull);
  // Create the filtering object
  pcl::ExtractIndices<pcl::PointXYZ> extract;

  int i = 0, nr_points = (int) cloud_filtered->points.size ();
  // While 30% of the original cloud is still there
  while (cloud_filtered->points.size () > 0.01 * nr_points)
  {

	  seg.setModelType (pcl::SACMODEL_LINE);
	  seg.setInputCloud (cloud_filtered);
	  seg.segment (*inliers, *coefficients);
	  proj.setModelType (pcl::SACMODEL_LINE);
	  proj.setIndices (inliers);
	  proj.setInputCloud (cloud_filtered);
	  proj.setModelCoefficients (coefficients);
	  proj.filter (*cloud_line);

	  pcl::PointXYZ minPoint(0,0,0);
	  pcl::PointXYZ maxPoint(0,0,0);

	  


	Eigen::Array4f min_p, max_p;
	min_p.setConstant (FLT_MAX);
	max_p.setConstant (-FLT_MAX);
	
	pcl::PointXYZ mini(0,0,0), maxi(0,0,0);
	// If the data is dense, we don't need to check for NaN
  if (cloud_line->is_dense)
  {
    for (size_t i = 0; i < cloud_line->points.size (); ++i)
    {
      pcl::Array4fMap pt = cloud_line->points[i].getArray4fMap ();
      //min_p = min_p.min (pt);
	  if(min_p[0] >= pt[0])
	  {
		  min_p[0] = pt[0];
		  mini.x = i;
	  }
	  if(min_p[1] >= pt[1])
	  {
		  min_p[1] = pt[1]; 
		  //mini.y = i;
	  }
	  if(min_p[2] >= pt[2])
	  {
			min_p[2] = pt[2];
			//mini.z = i;
	  }
      //max_p = max_p.max (pt);
	  if(max_p[0] <= pt[0])
	  {
		  max_p[0] = pt[0]; 
		  maxi.x = i;
	  }
	  if(max_p[1] <= pt[1])
	  {
		  max_p[1] = pt[1];
		  //maxi.y = i;
	  }
	  if(max_p[2] <= pt[2])
	  {
		  max_p[2] = pt[2];
		  //maxi.z = i;
	  }
    }
  }

  minPoint.x = cloud_line->points[mini.x].x; 
  minPoint.y = cloud_line->points[mini.x].y;
  minPoint.z = cloud_line->points[mini.x].z;
  maxPoint.x = cloud_line->points[maxi.x].x;
  maxPoint.y = cloud_line->points[maxi.x].y;
  maxPoint.z = cloud_line->points[maxi.x].z;

  

	  extract.setInputCloud (cloud_filtered);
      extract.setIndices (inliers);
      extract.setNegative (true);
      extract.filter (*cloud_line);
	  cloud_filtered.swap(cloud_line);

	  viewer.addArrow(minPoint,maxPoint, 0.3, 0.6, 0.9, 0.5,0.5,0.5, "arr"+boost::lexical_cast<std::string>(i));
	  ++i;
	  
  }
  //pcl::PCDWriter writer;
  //writer.write ("table_scene_mug_stereo_textured_hull.pcd", *cloud_hull, false);

  
  
  

  while (!viewer.wasStopped ())
  {
  viewer.spinOnce ();
  }
  return (0);
}