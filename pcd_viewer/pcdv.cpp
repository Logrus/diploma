#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <iostream>
#include <boost/thread/thread.hpp>
#include <pcl/visualization/pcl_visualizer.h>
#include <vtkPropPicker.h>
// For segmentation and extraction of planars
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
// Filters
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>

#include <cstdlib>


void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void);
void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void);

int
main (int argc, char** argv)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
	viewer->setBackgroundColor (0, 0, 0);
	viewer->initCameraParameters ();

	viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)&viewer);
	viewer->registerMouseCallback (mouseEventOccurred, (void*)&viewer);


	// Load file
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::io::loadPCDFile<pcl::PointXYZ> ("../../c1.pcd", *cloud); //* load the file
	//pcl::io::loadPLYFile<pcl::PointXYZ> ("mesh_1.ply", *cloud);
	
	// Pass through filter
	//pcl::PassThrough<pcl::PointXYZ> pass;
	//pass.setInputCloud (cloud);
	//pass.setFilterFieldName ("z");
	//pass.setFilterLimits (0.0, 1.0);
	//pass.filter (*cloud_filtered);
	//cloud.swap (cloud_filtered);

	// Downsample
	pcl::VoxelGrid<pcl::PointXYZ> sor;
	sor.setInputCloud (cloud);
	sor.setLeafSize (0.01f, 0.01f, 0.01f);
	sor.filter (*cloud_filtered);
	cloud.swap (cloud_filtered);

	// Remove outliers
	pcl::StatisticalOutlierRemoval<pcl::PointXYZ> outliers_filter;
	outliers_filter.setInputCloud (cloud);
	outliers_filter.setMeanK (50);
	outliers_filter.setStddevMulThresh (1.0);
	outliers_filter.filter (*cloud_filtered);
	cloud.swap (cloud_filtered);

	// Segmentation
	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
	pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
	// Create the segmentation object
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	// Optional
	seg.setOptimizeCoefficients (true);
	// Mandatory
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setMaxIterations (1000);
	seg.setDistanceThreshold (0.01);

	// Create the filtering object
	pcl::ExtractIndices<pcl::PointXYZ> extract;

	int i = 0, nr_points = (int) cloud->points.size ();
	// While 30% of the original cloud is still there
	while (cloud->points.size () > 0.3 * nr_points)
	{
		// Segment the largest planar component from the remaining cloud
		seg.setInputCloud (cloud);
		seg.segment (*inliers, *coefficients);
		if (inliers->indices.size () == 0)
		{
		std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
		break;
		}

		// Extract the inliers
		extract.setInputCloud (cloud);
		extract.setIndices (inliers);
		extract.setNegative (false);
		extract.filter (*cloud_filtered);
		//std::cerr << "PointCloud representing the planar component: " << cloud_filtered->width * cloud_filtered->height << " data points." << std::endl;

		int color1 = rand() % 255;
		int color2 = rand() % 255;
		int color3 = rand() % 255;
		
		
		pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> single_color(cloud_filtered, color1, color2, color3);
		viewer->addPointCloud<pcl::PointXYZ> (cloud_filtered, single_color, "cloud"+i);
		viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "cloud"+i);

		// Now we want to compute a bounding box
		Eigen::Vector4f centroid;
		pcl::compute3DCentroid(*cloud_filtered, centroid);
		Eigen::Matrix3f covariance;
		pcl::computeCovarianceMatrixNormalized(*cloud_filtered, centroid, covariance);
		Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
		Eigen::Matrix3f eigDx = eigen_solver.eigenvectors();
		eigDx.col(2) =  eigDx.col(0).cross(eigDx.col(1));

		Eigen::Matrix4f p2w(Eigen::Matrix4f::Identity());
		p2w.block<3,3>(0,0) = eigDx.transpose();
		p2w.block<3,1>(0,3) = -1.f * (p2w.block<3,3>(0,0) * centroid.head<3>());
		pcl::PointCloud<pcl::PointXYZ> cPoints;
		pcl::transformPointCloud(*cloud_filtered, cPoints, p2w);

		pcl::PointXYZ minPt, maxPt;
		pcl::getMinMax3D(cPoints, minPt, maxPt);
		const Eigen::Vector3f mean_diag = 0.5f * (maxPt.getVector3fMap() + minPt.getVector3fMap());

		const Eigen::Quaternionf qfinal(eigDx);
		const Eigen::Vector3f tfinal = eigDx*mean_diag + centroid.head<3>();
		
		viewer->addCube(tfinal, qfinal, maxPt.x - minPt.x, maxPt.y - minPt.y, maxPt.z - minPt.z, "cube"+i);
		std::cout<<"x"<<maxPt.x - minPt.x<<" y"<<maxPt.y - minPt.y<<" z"<< maxPt.z - minPt.z;
		
		
		viewer->addText ("Dimention x: " + boost::lexical_cast<std::string>(maxPt.x - minPt.x), 10, 60, "x");
		viewer->addText ("Dimention y: " + boost::lexical_cast<std::string>(maxPt.y - minPt.y), 10, 43, "y");
		viewer->addText ("Dimention z: " + boost::lexical_cast<std::string>(maxPt.z - minPt.z), 10, 30, "z");

		// Create the filtering object
		extract.setNegative (true);
		extract.filter (*cloud_filtered);
		cloud.swap (cloud_filtered);
		i++;
	}
	// Finally we render the rest of the cloud
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> single_color(cloud_filtered, 100, 100, 100);
	viewer->addPointCloud<pcl::PointXYZ> (cloud, single_color, "outliers");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "outliers");

	std::cout<<"Start visualizing...\n";
	// Visualization
	viewer->addCoordinateSystem (1.0, "global");
	
	std::cout<<"Initialized parameters...\n";

	while (!viewer->wasStopped ())
	{
		viewer->spinOnce (100);
		boost::this_thread::sleep (boost::posix_time::microseconds (100000));
	}
	return (0);
}

unsigned int text_id = 0;
void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getKeySym () == "r" && event.keyDown ())
  {
    std::cout << "r was pressed => removing all text" << std::endl;

    char str[512];
    for (unsigned int i = 0; i < text_id; ++i)
    {
      sprintf (str, "text#%03d", i);
      viewer->removeShape (str);
    }
    text_id = 0;
  }
}

void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getButton () == pcl::visualization::MouseEvent::LeftButton &&
      event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease)
  {
	  
	vtkSmartPointer<vtkPropPicker>  picker =
        vtkSmartPointer<vtkPropPicker>::New();
	picker->Pick(event.getX () , event.getY (), 0, viewer->getRendererCollection()->GetFirstRenderer());
	double* pos = picker->GetPickPosition();
      std::cout << "Pick position (world coordinates) is: "
                << pos[0] << " " << pos[1]
                << " " << pos[2] << std::endl;
 
      std::cout << "Picked actor: " << picker->GetActor() << std::endl;
    //char str[512];
    //sprintf (str, "text#%03d", text_id ++);
    //viewer->addText ("clicked here", event.getX (), event.getY (), str);
  }
}