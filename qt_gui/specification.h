#ifndef SPECIFICATION_H
#define SPECIFICATION_H
#include <QTextDocument>
#include <QTextCursor>
#include <QtGui>

// Поля
// Порядковый номер
// Артикул
// Наименование по каталогу производителя
// Наименование по каталогу производителя на русском языке
// Единица измерения
// Количество
class specification
{
public:
    specification();
    ~specification();

    void addObject(const QString &art,
                   const QString &name_catalog,
                   const QString &name_catalog_rus,
                   const QString &gauge,
                   int quantity
                   );
    void write(const QString &fileName);
    void print(QPrinter &Printer);

private:
    QTextBlockFormat centerFormat;
    QTextCharFormat boldFormat;
    QTextCharFormat normalFormat;
    QTextDocument * const m_document;
    QTextCursor m_cursor;
    int number;
};

#endif // SPECIFICATION_H
