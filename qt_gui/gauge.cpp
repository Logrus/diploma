#include "gauge.h"
#include <algorithm>
#include "build/ui_gauge.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <cmath>
#include <QPrinter>
#include <QPrintDialog>
#include <QFile>
#include <QDataStream>
#include <QMap>

#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <vtkRenderWindow.h>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/common/transforms.h>
#include "gaugevisualizerinteractorstyle.h"
#include "dialog.h"
#include <vtkPropPicker.h>
#include <pcl/registration/icp.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/project_inliers.h>
#include <vtkOBJReader.h>
#include "parameters.h"
#include "overloads.h"
#include "specification.h"

boost::shared_ptr<pcl::visualization::PCLVisualizer> pviz (new pcl::visualization::PCLVisualizer ("test_viz", false));
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_xyz (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::PCLPointCloud2 loadCloud;

void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void);
void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void);
void pickPointEventOccured (const pcl::visualization::PointPickingEvent& event,
                        void* viewer_void);

enum interactive_mode {RULLER, OBJECTS, NONE} currentMode;
int point_counter=0;
struct manualRuller
{
    pcl::PointXYZRGB first;
    pcl::PointXYZRGB second;
    double lenghth;
} manrul;

struct PCD
{
  pcl::PointCloud <pcl::PointXYZRGB>::Ptr cloud; // This cloud can have any fields
  std::string name; // Name of the cloud
  PCD() : cloud (new pcl::PointCloud <pcl::PointXYZRGB>) {} // Allocate memory
  // Methods
  bool operator==(const PCD &f) const {return this->name == f.name;}
};

std::vector<PCD, Eigen::aligned_allocator<PCD> > data_1;

struct boxes_measures
{
    float m_x,m_y,m_z;
    Eigen::Quaternionf qfinal;
    Eigen::Vector3f tfinal;
};

struct arrows_measures
{
    pcl::PointXYZRGB first;
    pcl::PointXYZRGB second;
};

//union project_object
//{
//    boxes_measures bm;
//    arrows_measures am;
//};

// std::map<std::string,project_object> project;



QMap<QString, parameters > projects;

std::map<std::string, boxes_measures> boxes_m;

vtkSmartPointer<vtkActor> current_object = vtkSmartPointer<vtkActor>::New();

Gauge::Gauge(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Gauge)
{
    ui->setupUi(this);
    statProgressBar = new QProgressBar(this);
    ui->statusBar->addPermanentWidget(statProgressBar,1);

    ui->treeWidget->setColumnCount(1);
    ui->treeWidget->setHeaderLabel(trUtf8("Категории"));
    addRoot(trUtf8("Облака точек"));
    addRoot(trUtf8("Материалы"));
    addRoot(trUtf8("Измерения"));

    // Clasterization settings
    ui->hSliderMinCluster->setMaximum(1000);
    ui->hSliderMinCluster->setMinimum(1);
    ui->hSliderAngleRegionGrowing->setMaximum(100);
    ui->hSliderAngleRegionGrowing->setMinimum(5);
    ui->hSliderSetNeighbour->setMaximum(100);
    ui->hSliderSetNeighbour->setMinimum(5);

    // Registration settings
    ui->hs_Registration_radius->setMaximum(1000);
    ui->hs_Registration_radius->setMinimum(1);
    ui->hs_Registration_radius->setValue(5);
    ui->display_Registration_radius->setText(QString::number(ui->hs_Registration_radius->value()*0.001));

    ui->hs_Registration_max_iterations->setMaximum(100);
    ui->hs_Registration_max_iterations->setMinimum(1);
    ui->hs_Registration_max_iterations->setValue(50);
    ui->display_Registration_max_iterations->setText(QString::number(ui->hs_Registration_max_iterations->value()));

    ui->hs_Registration_epsilon->setMaximum(10);
    ui->hs_Registration_epsilon->setMinimum(1);
    ui->hs_Registration_epsilon->setValue(8);
    ui->display_Registration_epsilon->setText(QString::number(1/qPow(10,ui->hs_Registration_epsilon->value())));

    ui->hs_Registration_squared_error->setMaximum(10);
    ui->hs_Registration_squared_error->setMinimum(1);
    ui->hs_Registration_squared_error->setValue(1);
    ui->display_Registration_squared_error->setText(QString::number(ui->hs_Registration_squared_error->value()));

    // Set default values for sliders
    int minClusterSize = 0, neighbours = 0;
    double angle = 0.0;
    try{
       QSettings gauge_settings("../config.ini", QSettings::IniFormat);
       minClusterSize = gauge_settings.value("Region_growing/min_cluster_size", 300).toInt();
       neighbours = gauge_settings.value("Region_growing/neighbours", 30).toInt();
       angle = gauge_settings.value("Region_growing/angle", 3.0).toDouble();
    } catch(...){
        try{
            QSettings gauge_settings("config.ini", QSettings::IniFormat);
            minClusterSize = gauge_settings.value("Region_growing/min_cluster_size", 300).toInt();
            neighbours = gauge_settings.value("Region_growing/neighbours", 30).toInt();
            angle = gauge_settings.value("Region_growing/angle", 3.0).toDouble();
        }
        catch(...){
        }
    }
    ui->hSliderMinCluster->setValue(minClusterSize);
    ui->hSliderAngleRegionGrowing->setValue(static_cast<int>(angle * 10));
    ui->hSliderSetNeighbour->setValue(neighbours);
    QString s;
    s.setNum(ui->hSliderMinCluster->value());
    ui->label->setText(s);
    s.setNum(ui->hSliderAngleRegionGrowing->value()*0.1, 'g', 2);
    ui->label_2->setText(s);
    s.setNum(ui->hSliderSetNeighbour->value());
    ui->label_3->setText(s);


   vtkSmartPointer<vtkRenderWindow> renderWindow = pviz->getRenderWindow();
   ui->widget->SetRenderWindow (renderWindow);
   pcl::visualization::PCLVisualizerInteractorStyle* style = new
           pcl::visualization::GaugeVisualizerInteractorStyle;
   //style->Initialize();
   //style->setRendererCollection(pviz->getRendererCollection());
   //style->setCloudActorMap(pviz->getCloudActorMap());
   //style->UseTimersOn();
   //style->setUseVbos(false);
   //style->registerKeyboardCallback(boost::bind (keyboardEventOccurred, _1, (void*)&pviz));
   //style->registerMouseCallback(boost::bind (mouseEventOccurred, _1, (void*)&pviz));
   //style->registerPointPickingCallback(boost::bind(pickPointEventOccured, _1, (void*)&pviz));
   //style->setKeyboardModifier(pcl::visualization::INTERACTOR_KB_MOD_SHIFT);

   //pviz->setupInteractor (ui->widget->GetInteractor (), ui->widget->GetRenderWindow (), style);
   pviz->setupInteractor(ui->widget->GetInteractor(), ui->widget->GetRenderWindow());
   pviz->registerKeyboardCallback(keyboardEventOccurred, (void*)&pviz);
   pviz->registerMouseCallback(mouseEventOccurred, (void*)&pviz);
   pviz->registerPointPickingCallback(pickPointEventOccured, (void*)&pviz);
   pviz->getInteractorStyle()->setKeyboardModifier(pcl::visualization::INTERACTOR_KB_MOD_SHIFT);
   pviz->setBackgroundColor(0, 0, 0);
   pviz->setCameraPosition(1.54945, -4.29104, -11.8383, -0.0411026, -0.938664, 0.342375);
   pviz->setShowFPS(false);

   pviz->addText("X:", 20, 60, "BBSizeX");
   pviz->addText("Y:", 20, 40, "BBSizeY");
   pviz->addText("Z:", 20, 20, "BBSizeZ");

   ui->widget->show();

   // Load list of objects
   //QSettings gauge_objects("../objects.ini", QSettings::IniFormat);
   //gauge_objects.setIniCodec("utf-8");
   //QString sample = gauge_objects.value("Розетки/Свойство").toString();
   //ui->listWidget->addItem(sample);
   ui->listWidget->addItem(trUtf8("Розетки"));
   //connect(this, SIGNAL(fileLoadingStart(int)), this->statProgressBar, SLOT(setMaximum(int)));
}

Gauge::~Gauge()
{
    delete ui;
}

void Gauge::on_action_triggered()
{
    this->close();
}

void Gauge::on_action_2_triggered()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this,trUtf8("Открыть pcd"), "../../",
                                                          trUtf8("Файлы облаков точек (*.pcd)"));

    if (fileNames.size() == 0) return; // If no files choosen then exit

    for (int i = 0; i < fileNames.size(); i++)
    {
        this->loadPCD(fileNames.at(i));
    }
}

void Gauge::on_actionRegion_Growing_triggered()
{
    QTreeWidgetItem *item = ui->treeWidget->currentItem();
    if (!item) return;
    QTreeWidgetItem *parent = item->parent();
    if(!parent) return;
    if (!(parent == ui->treeWidget->topLevelItem(0))) return;
    if (!item->isSelected()) return;
    std::vector<PCD, Eigen::aligned_allocator<PCD> >::iterator it;
    PCD m; m.name = item->text(0).toStdString();
    it = std::find (data_1.begin(), data_1.end(), m);
    cloud_xyz.swap((*it).cloud);

    // Create tree
    pcl::search::Search<pcl::PointXYZRGB>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZRGB> > (new pcl::search::KdTree<pcl::PointXYZRGB>);
    pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> normal_estimator;
    normal_estimator.setSearchMethod (tree);
    normal_estimator.setInputCloud (cloud_xyz);
    normal_estimator.setKSearch (50);
    normal_estimator.compute (*normals);

    // Region growing part
    pcl::RegionGrowing<pcl::PointXYZRGB, pcl::Normal> reg;
    reg.setMinClusterSize (ui->hSliderMinCluster->value());
    reg.setMaxClusterSize (1000000);
    reg.setSearchMethod (tree);
    reg.setNumberOfNeighbours (ui->hSliderSetNeighbour->value());
    reg.setInputCloud (cloud_xyz);
    //reg.setIndices (indices);
    reg.setInputNormals (normals);
    reg.setSmoothnessThreshold (ui->hSliderAngleRegionGrowing->value()*0.1 / 180.0 * M_PI);
    reg.setCurvatureThreshold (1.0);

    std::vector <pcl::PointIndices> clusters;
    //boost::thread t(boost::bind(&pcl::RegionGrowing<pcl::PointXYZRGB, pcl::Normal>::extract, clusters));
    reg.extract (clusters);
    colored_cloud = reg.getColoredCloud ();
    //pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(colored_cloud);
    //pviz->addPointCloud<pcl::PointXYZRGB> (colored_cloud, rgb, "sample cloud");
    //pviz->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");

    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    pcl::PointIndices::Ptr indecies (new pcl::PointIndices ());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);
    extract.setInputCloud(cloud_xyz);
    int arrow_indices = 0;
    int i=0;
    for ( int cluster_index = 0 ; cluster_index < clusters.size(); cluster_index++)
    {
        std::cout<<"Cluster index: "<<cluster_index<<"\n";
        indecies->indices = clusters[cluster_index].indices;

        extract.setIndices(indecies);
        extract.setNegative (false);
        extract.filter(*cloud_filtered);

        if(ui->comboBox->currentIndex() == 0)
        {
            // Now we want to compute a bounding box
            Eigen::Vector4f centroid;
            pcl::compute3DCentroid(*cloud_filtered, centroid);
            Eigen::Matrix3f covariance;
            pcl::computeCovarianceMatrixNormalized(*cloud_filtered, centroid, covariance);
            Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
            Eigen::Matrix3f eigDx = eigen_solver.eigenvectors();
            eigDx.col(2) =  eigDx.col(0).cross(eigDx.col(1));

            Eigen::Matrix4f p2w(Eigen::Matrix4f::Identity());
            p2w.block<3,3>(0,0) = eigDx.transpose();
            p2w.block<3,1>(0,3) = -1.f * (p2w.block<3,3>(0,0) * centroid.head<3>());
            pcl::PointCloud<pcl::PointXYZRGB> cPoints;
            pcl::transformPointCloud(*cloud_filtered, cPoints, p2w);

            pcl::PointXYZRGB minPt, maxPt;
            pcl::getMinMax3D(cPoints, minPt, maxPt);
            const Eigen::Vector3f mean_diag = 0.5f * (maxPt.getVector3fMap() + minPt.getVector3fMap());

            const Eigen::Quaternionf qfinal(eigDx);
            const Eigen::Vector3f tfinal = eigDx*mean_diag + centroid.head<3>();

            pviz->addCube(tfinal, qfinal, maxPt.x - minPt.x, maxPt.y - minPt.y, maxPt.z - minPt.z, "cube"+boost::lexical_cast<std::string>(i));

            boxes_measures m;
            m.m_x = (maxPt.x - minPt.x);
            m.m_y = (maxPt.y - minPt.y);
            m.m_z = (maxPt.z - minPt.z);
            m.tfinal = tfinal;
            m.qfinal = qfinal;
            boxes_m["cube"+boost::lexical_cast<std::string>(i)] = m;
            QTreeWidgetItem *item = new QTreeWidgetItem();
            item->setText(0,"cube" + QString::number(i));
            ui->treeWidget->topLevelItem(2)->addChild(item);
        }
        else
        {
            // Otherwise we want a convex hull
            pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
            pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_projected (new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_temp (new pcl::PointCloud<pcl::PointXYZRGB>);
            // Create the segmentation object
            pcl::SACSegmentation<pcl::PointXYZRGB> seg;
            // Optional
            seg.setOptimizeCoefficients (true);
            // Mandatory
            seg.setModelType (pcl::SACMODEL_PLANE);
            seg.setMethodType (pcl::SAC_RANSAC);
            seg.setDistanceThreshold (0.01);

            seg.setInputCloud (cloud_filtered);
            seg.segment (*inliers, *coefficients);

            // Project the model inliers
            pcl::ProjectInliers<pcl::PointXYZRGB> proj;
            proj.setModelType (pcl::SACMODEL_PLANE);
            proj.setIndices (inliers);
            proj.setInputCloud (cloud_filtered);
            proj.setModelCoefficients (coefficients);
            proj.filter (*cloud_projected);

            // Create a Concave Hull representation of the projected inliers
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_hull (new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::ConcaveHull<pcl::PointXYZRGB> chull;
            chull.setInputCloud (cloud_projected);
            chull.setAlpha (0.1);
            chull.reconstruct (*cloud_hull);

            std::cerr << "Concave hull has: " << cloud_hull->points.size ()
                      << " data points." << std::endl;

            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_line (new pcl::PointCloud<pcl::PointXYZRGB>);

            cloud_temp.swap(cloud_hull);
            // Create the filtering object
            pcl::ExtractIndices<pcl::PointXYZRGB> extract_line;


            int i = 0, nr_points = (int) cloud_temp->points.size ();
            // While 30% of the original cloud is still there
            while (cloud_temp->points.size () > 0.1 * nr_points)
            {

                seg.setModelType (pcl::SACMODEL_LINE);
                seg.setInputCloud (cloud_temp);
                seg.segment (*inliers, *coefficients);
                proj.setModelType (pcl::SACMODEL_LINE);
                proj.setIndices (inliers);
                proj.setInputCloud (cloud_temp);
                proj.setModelCoefficients (coefficients);
                proj.filter (*cloud_line);

                pcl::PointXYZRGB minPoint(0,0,0);
                pcl::PointXYZRGB maxPoint(0,0,0);

                Eigen::Array4f min_p, max_p;
                min_p.setConstant (FLT_MAX);
                max_p.setConstant (-FLT_MAX);

                pcl::PointXYZRGB mini(0,0,0), maxi(0,0,0);
                // If the data is dense, we don't need to check for NaN
                if (cloud_line->is_dense)
                {
                    for (size_t i = 0; i < cloud_line->points.size (); ++i)
                    {
                        pcl::Array4fMap pt = cloud_line->points[i].getArray4fMap ();
                        //min_p = min_p.min (pt);
                        if(min_p[0] >= pt[0])
                        {
                            min_p[0] = pt[0];
                            mini.x = i;
                        }
                        if(min_p[1] >= pt[1])
                        {
                            min_p[1] = pt[1];
                            //mini.y = i;
                        }
                        if(min_p[2] >= pt[2])
                        {
                            min_p[2] = pt[2];
                            //mini.z = i;
                        }
                        //max_p = max_p.max (pt);
                        if(max_p[0] <= pt[0])
                        {
                            max_p[0] = pt[0];
                            maxi.x = i;
                        }
                        if(max_p[1] <= pt[1])
                        {
                            max_p[1] = pt[1];
                            //maxi.y = i;
                        }
                        if(max_p[2] <= pt[2])
                        {
                            max_p[2] = pt[2];
                            //maxi.z = i;
                        }
                    }
                }

                minPoint.x = cloud_line->points[mini.x].x;
                minPoint.y = cloud_line->points[mini.x].y;
                minPoint.z = cloud_line->points[mini.x].z;
                maxPoint.x = cloud_line->points[maxi.x].x;
                maxPoint.y = cloud_line->points[maxi.x].y;
                maxPoint.z = cloud_line->points[maxi.x].z;

                extract_line.setInputCloud (cloud_temp);
                extract_line.setIndices (inliers);
                extract_line.setNegative (true);
                extract_line.filter (*cloud_line);
                cloud_temp.swap(cloud_line);


                pviz->addArrow(minPoint,maxPoint, 0.3, 0.6, 0.9, 0.5,0.5,0.5, "arr"+boost::lexical_cast<std::string>(arrow_indices));

                QTreeWidgetItem *item = new QTreeWidgetItem();
                item->setText(0,"arr" + QString::number(arrow_indices));
                ui->treeWidget->topLevelItem(2)->addChild(item);
                ++i; ++arrow_indices;
              }
        }


        indecies->indices.clear();
        i++;
    }
}

void Gauge::on_hSliderMinCluster_actionTriggered(int action)
{
    QString s;
    s.setNum(ui->hSliderMinCluster->value());
    ui->label->setText(s);
}

void Gauge::on_hSliderAngleRegionGrowing_actionTriggered(int action)
{
    QString s;
    s.setNum(ui->hSliderAngleRegionGrowing->value()*0.1, 'g', 2);
    ui->label_2->setText(s);
}

void Gauge::on_hSliderSetNeighbour_actionTriggered(int action)
{
    QString s;
    s.setNum(ui->hSliderSetNeighbour->value());
    ui->label_3->setText(s);
}

void Gauge::on_pushButton_clicked()
{
    Gauge::on_actionRegion_Growing_triggered();
}

/*unsigned int text_id = 0;*/
void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if ((event.getKeySym () == "p" && event.keyDown ()) ||
          (event.getKeySym () == "P" && event.keyDown ()))
  {
       // Maybe implement printing someday
  }
}

void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void)
{
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
    if (event.getButton () == pcl::visualization::MouseEvent::LeftButton &&
            event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease)
    {
        vtkSmartPointer<vtkPropPicker>  picker =
                vtkSmartPointer<vtkPropPicker>::New();
        picker->Pick(event.getX () , event.getY (), 0, viewer->getRendererCollection()->GetFirstRenderer());
        double* pos = picker->GetPickPosition();

        std::cout << "Pick position (world coordinates) is: "
                  << pos[0] << " " << pos[1]
                  << " " << pos[2] << std::endl;

        vtkSmartPointer<vtkActor> act (picker->GetActor());
        current_object = act;
        std::cout << "Picked actor: " << act << std::endl;
        pcl::visualization::ShapeActorMapPtr actors = viewer->getShapeActorMap();
        pcl::visualization::ShapeActorMap::iterator it;
        for(it = actors->begin(); it != actors->end(); it++)
        {

            //std::cout << it->first << "\n"; // shows all shapes
            if ( it->second == act)
            {
                std::map<std::string, boxes_measures>::const_iterator it_box = boxes_m.find(it->first);
                viewer->updateText("X:" + boost::lexical_cast<std::string>(it_box->second.m_x), 20, 60, "BBSizeX");
                viewer->updateText("Y:" + boost::lexical_cast<std::string>(it_box->second.m_y), 20, 40, "BBSizeY");
                viewer->updateText("Z:" + boost::lexical_cast<std::string>(it_box->second.m_z), 20, 20, "BBSizeZ");
            }

        }
    }
}

void Gauge::on_action_4_triggered()
{
    Dialog AboutWindow;
    AboutWindow.setModal(true);
    AboutWindow.exec();
}

void Gauge::addRoot(QString name)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
    item->setText(0,name);
    ui->treeWidget->addTopLevelItem(item);


}
void Gauge::addChild(QTreeWidgetItem *parent, QString name)
{
    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setText(0,name);
    parent->addChild(item);
}

void Gauge::on_pushButton_2_clicked()
{
    QTreeWidgetItem *item = ui->treeWidget->currentItem();
    if (!item) return;
    QTreeWidgetItem *parent = item->parent();
    // We need to empty data_1 and other structures
    if (!parent) return;

    // Is it pointclouds?
    if(parent == ui->treeWidget->topLevelItem(0))
    {
        PCD m; m.name = ui->treeWidget->currentItem()->text(0).toStdString();
        data_1.erase(std::remove(data_1.begin(), data_1.end(), m)); // Free memory
        pviz->removeShape(item->text(0).toStdString()); // Clear screen
        parent->removeChild(item); // Remove from tree
        pviz->updateCamera(); // Update camera
    }
    else if (parent == ui->treeWidget->topLevelItem(1))
    {}
    else
    {
        pviz->removeShape(item->text(0).toStdString()); // Clear screen
        parent->removeChild(item); // Remove from tree
        pviz->updateCamera(); // Update camera
    }
}

void Gauge::on_action_8_triggered()
{
    if(data_1.size() == 0) return;
    if(data_1.size() != 2)
    {
        QMessageBox::warning(this,
                             trUtf8("Несоответствие количества облаков точек!"),
                             trUtf8("Должно быть загружено только 2 обкала."));
        return;
    }
    int childs = ui->treeWidget->topLevelItem(0)->childCount();
    for (int i = 0; i<=childs; i++)
        ui->treeWidget->topLevelItem(0)->removeChild(ui->treeWidget->topLevelItem(0)->child(0));

    pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
    icp.setInputCloud(data_1[0].cloud);
    icp.setInputTarget(data_1[1].cloud);

    // Set the max correspondence distance to 5cm (e.g., correspondences with higher distances will be ignored)
    icp.setMaxCorrespondenceDistance (ui->display_Registration_radius->text().toDouble());
    // Set the maximum number of iterations (criterion 1)
    icp.setMaximumIterations (ui->display_Registration_max_iterations->text().toDouble());
    // Set the transformation epsilon (criterion 2)
    icp.setTransformationEpsilon (ui->display_Registration_epsilon->text().toDouble());
    // Set the euclidean distance difference epsilon (criterion 3)
    icp.setEuclideanFitnessEpsilon (ui->display_Registration_squared_error->text().toDouble());

    //pcl::PointCloud<pcl::PointXYZRGB> Final;
    icp.align(*cloud_xyz);
    pviz->removeAllPointClouds();
    data_1.clear();
    PCD m;
    m.name = "RegisteredCloud";
    m.cloud.swap(cloud_xyz);
    data_1.push_back(m);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> color(data_1[0].cloud, 255, 255, 255);
    pviz->addPointCloud(data_1[0].cloud, color, m.name);

    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setText(0,"RegisteredCloud");
    ui->treeWidget->topLevelItem(0)->addChild(item);
}

void
pickPointEventOccured (const pcl::visualization::PointPickingEvent& event, void* viewer_void)
{
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
    if (event.getPointIndex () == -1)
        return;
    pcl::PointXYZRGB current_point;
    event.getPoint(current_point.x, current_point.y, current_point.z);
    int rrand =rand() % 10000;
    if(currentMode == RULLER)
    {
        if(!point_counter)
        {
            manrul.first = current_point;
            ++point_counter;
        }
        else if(point_counter==1)
        {
            manrul.second = current_point;
            pviz->addArrow(manrul.first,manrul.second, 0.3, 0.6, 0.9, 0.9,0.2,0.2, "ManualMeasurment"+boost::lexical_cast<std::string>(rrand));
            point_counter=0;
            //QTreeWidgetItem *item = new QTreeWidgetItem();
            //item->setText(0,temp_name);
            //ui->treeWidget->topLevelItem(0)->addChild(item);
        }
    }

    if (currentMode == OBJECTS)
    {
        vtkSmartPointer<vtkOBJReader> reader =
                vtkSmartPointer<vtkOBJReader>::New();
        reader->SetFileName("./objects/socket.obj");
        reader->Update();
        vtkPolyData * object3d = reader->GetOutput();


        //Create a mapper and actor
        vtkSmartPointer<vtkPolyDataMapper> mapper =
                vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInput (object3d);

        vtkSmartPointer<vtkActor> actor =
                vtkSmartPointer<vtkActor>::New();
        actor->SetMapper(mapper);

        actor->SetOrientation(0,0,0);
        actor->SetOrigin(current_point.x, current_point.y, current_point.z);
        actor->SetPosition(0, 0, 0);
        actor->SetScale(0.01);

        std::stringstream buff;
        buff << actor;
        QString name;
        name = buff.str().c_str();
        //name.fromStdString(buff.str());
        qDebug () << "NAMAE: " << name ;
        cout<< "NAME: "<< buff.str() << "\n";
        parameters p;
        actor->GetOrientation(p.orientation);
        actor->GetOrigin(p.origin);
        actor->GetScale(p.scale);

        projects.insert(name, p);
        std::cout << "PS:" << projects.size();
        //actor->GetCenter();
        //actor->GetOrientation();
        //actor->GetPosition(); // double[3]
        //actor->GetScale(); //double

        viewer->getRendererCollection()->GetFirstRenderer()->AddActor(actor);
    }


}

void Gauge::on_pushButton_5_clicked()
{
    double angleX, angleY, angleZ;

    if (ui->lineEdit_angleX->text() != "")
        angleX = ui->lineEdit_angleX->text().toDouble();
    else
        angleX = 0;

    if (ui->lineEdit_angleY->text() != "")
        angleY = ui->lineEdit_angleY->text().toDouble();
    else
        angleY = 0;

    if (ui->lineEdit_angleZ->text() != "")
        angleZ = ui->lineEdit_angleZ->text().toDouble();
    else
        angleZ = 0;

    if(current_object == 0)
        return;

    current_object->RotateX(angleX);
    current_object->RotateY(angleY);
    current_object->RotateZ(angleZ);
    std::stringstream buff;
    buff << current_object;
    QString name;
    name = buff.str().c_str();
    QMap<QString, parameters>::iterator it;
    it = projects.find(name);
    //if (it) std::cout << "We found something\n";
    current_object->GetOrientation(it->orientation);
    std::cout<< "Gotten orientation " << it->orientation[0] << " "
             << it->orientation[1] << " " << it->orientation[2] << "\n";
    pviz->updateCamera();

    //pviz->updateShapePose(current_object, transform_matrix);
}

void Gauge::on_listWidget_itemChanged(QListWidgetItem *item)
{

}

void Gauge::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    ui->label_chosen->setText(current->text());
    currentMode = OBJECTS;
}

void Gauge::on_pushButton_6_clicked()
{
    ui->label_chosen->setText("");
    ui->listWidget->clearSelection();
    ui->listWidget->clearFocus();
    currentMode = NONE;
    int point_counter;
}

void Gauge::on_pushButton_7_clicked()
{
    ui->label_chosen->setText(trUtf8("Линейка"));
    currentMode = RULLER;
}

void Gauge::on_hs_Registration_max_iterations_actionTriggered(int action)
{
    ui->display_Registration_max_iterations->setText(QString::number(ui->hs_Registration_max_iterations->value()));
}

void Gauge::on_hs_Registration_epsilon_actionTriggered(int action)
{
    ui->display_Registration_epsilon->setText(QString::number(1/qPow(10,ui->hs_Registration_epsilon->value())));
}

void Gauge::on_hs_Registration_squared_error_actionTriggered(int action)
{
    ui->display_Registration_squared_error->setText(QString::number(ui->hs_Registration_squared_error->value()));
}

void Gauge::on_hs_Registration_radius_actionTriggered(int action)
{
    ui->display_Registration_radius->setText(QString::number(ui->hs_Registration_radius->value()*0.001));
}

void Gauge::on_pushButton_Registration_start_clicked()
{
    Gauge::on_action_8_triggered();
}

void Gauge::on_action_5_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    trUtf8("Сохранить проект"), "",
                                                    trUtf8("Файл проекта (*.txt)"));
    if (fileName.isEmpty())
        return;
    else {
        // We need to save project
        QFile file(fileName);
        if(!file.open(QIODevice::WriteOnly))
        {
            return;
        }
        QDataStream out(&file);
        out.setVersion(QDataStream::Qt_4_8);

        out << projects;

        file.flush();
        file.close();
    }


}

void Gauge::on_action_3_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,trUtf8("Открыть проект"), "",
                                                    trUtf8("Файлы проекта (*.txt)"));
    if (fileName.isEmpty())
        return;
    else {
        // We need to save project
        QFile file(fileName);
        if(!file.open(QIODevice::ReadOnly))
        {
            return;
        }
        QDataStream in(&file);
        in.setVersion(QDataStream::Qt_4_8);

        in >> projects;

        //file.flush();
        file.close();

        QMap<QString, parameters >::iterator it;
        for (it = projects.begin(); it != projects.end(); ++it)
        {
            vtkSmartPointer<vtkOBJReader> reader =
                    vtkSmartPointer<vtkOBJReader>::New();
            reader->SetFileName("./objects/socket.obj");
            reader->Update();
            vtkPolyData * object3d = reader->GetOutput();


            //Create a mapper and actor
            vtkSmartPointer<vtkPolyDataMapper> mapper =
                    vtkSmartPointer<vtkPolyDataMapper>::New();
            mapper->SetInput (object3d);

            vtkSmartPointer<vtkActor> actor =
                    vtkSmartPointer<vtkActor>::New();
            actor->SetMapper(mapper);

            actor->SetOrientation(it->orientation);
            actor->SetOrigin(it->origin);
            actor->SetPosition(0, 0, 0);
            actor->SetScale(it->scale);

            pviz->getRendererCollection()->GetFirstRenderer()->AddActor(actor);
        }
    }
}

void Gauge::on_action_7_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    trUtf8("Сохранить файл спецификации"), "",
                                                    trUtf8("Файл спецификации (*.odt)"));
    specification spec;
    if(projects.size() != 0)
    spec.addObject("771040",
                   trUtf8("Силовая розетка с лицевой панелью и рамкой 2К+З, White"),
                   trUtf8("Силовая розетка с лицевой панелью и рамкой 2К+З, White"),
                   trUtf8("шт."),
                   projects.size());
    spec.write(fileName);
}

void Gauge::on_action_6_triggered()
{
    specification spec;
    if(projects.size() != 0)
    spec.addObject("771040",
                   trUtf8("Силовая розетка с лицевой панелью и рамкой 2К+З, White"),
                   trUtf8("Силовая розетка с лицевой панелью и рамкой 2К+З, White"),
                   trUtf8("шт."),
                   projects.size());
    QPrinter Printer(QPrinter::HighResolution);
    QPrintDialog PrintDialog(&Printer, this);
    if (PrintDialog.exec())
        spec.print(Printer);

}

void Gauge::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->label_chosen->setText(item->text());
    currentMode = OBJECTS;
}

void Gauge::openFile(char *str)
{
    QString fileName(str);
    this->loadPCD(fileName);
}

void Gauge::loadPCD(const QString &fileName)
{
    // Loading files one by one
    PCD m;

    // Get the name of a file out of the FilePath
    int inx = fileName.lastIndexOf("\\"); // Find last occurance of / in the string
    QString cloudName = fileName.mid(inx+1); // Take everyting from that / to the end, e.g. /cloud.pcd

    // TODO: We need to account for cyrilic path
    if (pcl::io::loadPCDFile (fileName.toStdString(), loadCloud) == -1)
    {
        QMessageBox::warning(this,
                             trUtf8("Файлы не загружены"),
                             trUtf8("При загрузке произошла ошибка!<br>"
                                    "В пути к файлу должны быть <b>только латинские</b> "
                                    "символы."));
        return;
    }

    // File is loaded successfuly
    std::vector<int> indices;
    m.name = cloudName.toStdString(); // Set found cloud name
    // We need to do smth like m.cloud = loadCloud whick looks like this:
    pcl::fromPCLPointCloud2(loadCloud, *m.cloud);
    pcl::removeNaNFromPointCloud(*m.cloud,*m.cloud, indices);
    if (pcl::getFieldsList(loadCloud) == "x y z")
    {
        int color1 = rand() % 255; int color2 = rand() % 255; int color3 = rand() % 255;
//        if (fileNames.size() == 1 )
//        {
//            cloud_xyz.swap(m.cloud);
//            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> single_color(cloud_xyz, color1, color2, color3);
//            pviz->addPointCloud<pcl::PointXYZRGB>(cloud_xyz, single_color, m.name);
//        }
//        else
        {
            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> single_color(m.cloud, color1, color2, color3);
            pviz->addPointCloud<pcl::PointXYZRGB>(m.cloud, single_color, m.name);
        }

    }
    else // x y z rgb
    {
//        if (fileNames.size() == 1 )
//        {
//            cloud_xyz.swap(m.cloud);
//            pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud_xyz);
//            pviz->addPointCloud<pcl::PointXYZRGB>(cloud_xyz, rgb, m.name);
//        }
//        else
        {
            pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(m.cloud);
            pviz->addPointCloud<pcl::PointXYZRGB>(m.cloud, rgb, m.name);
        }
    }
    data_1.push_back (m);
    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setText(0,cloudName);
    ui->treeWidget->topLevelItem(0)->addChild(item);
}


void Gauge::removeItem(const QString &itemName)
{

}

void Gauge::on_button_DeleteAll_clicked()
{
    QTreeWidgetItem *item = ui->treeWidget->currentItem();
    if(!item) return;
    QTreeWidgetItem *parent = item->parent();
    // We need to empty data_1 and other structures
    if (!parent) return;

    // Is it pointclouds?
    if(parent == ui->treeWidget->topLevelItem(0))
    {
        data_1.clear();
        pviz->removeAllPointClouds();
        int c = parent->childCount();
        for (int i=c; i!=0; i--)
            parent->takeChild(0);
        pviz->updateCamera(); // Update camera
    }
    else if (parent == ui->treeWidget->topLevelItem(1))
    {}
    else
    {
        int c = parent->childCount();
        for (int i=c; i!=0; i--)
            pviz->removeShape(parent->takeChild(0)->text(0).toStdString());

        pviz->updateCamera(); // Update camera
    }

}
