/********************************************************************************
** Form generated from reading UI file 'gauge.ui'
**
** Created: Mon 31. Mar 16:59:35 2014
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAUGE_H
#define UI_GAUGE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>
#include <QVTKWidget.h>
QT_BEGIN_NAMESPACE

class Ui_Gauge
{
public:
    QAction *action;
    QAction *action_2;
    QWidget *centralWidget;
    QVTKWidget *widget;
    QProgressBar *progressBar;
    QMenuBar *menuBar;
    QMenu *menu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Gauge)
    {
        if (Gauge->objectName().isEmpty())
            Gauge->setObjectName(QString::fromUtf8("Gauge"));
        Gauge->resize(713, 373);
        action = new QAction(Gauge);
        action->setObjectName(QString::fromUtf8("action"));
        action_2 = new QAction(Gauge);
        action_2->setObjectName(QString::fromUtf8("action_2"));
        centralWidget = new QWidget(Gauge);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        widget = new QVTKWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(10, 10, 601, 271));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(10, 290, 118, 23));
        progressBar->setValue(24);
        Gauge->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Gauge);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 713, 21));
        menu = new QMenu(menuBar);
        menu->setObjectName(QString::fromUtf8("menu"));
        Gauge->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Gauge);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        Gauge->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Gauge);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Gauge->setStatusBar(statusBar);

        menuBar->addAction(menu->menuAction());
        menu->addAction(action_2);
        menu->addAction(action);

        retranslateUi(Gauge);

        QMetaObject::connectSlotsByName(Gauge);
    } // setupUi

    void retranslateUi(QMainWindow *Gauge)
    {
        Gauge->setWindowTitle(QApplication::translate("Gauge", "Gauge", 0, QApplication::UnicodeUTF8));
        action->setText(QApplication::translate("Gauge", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
        action_2->setText(QApplication::translate("Gauge", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214 \321\204\320\260\320\271\320\273", 0, QApplication::UnicodeUTF8));
        menu->setTitle(QApplication::translate("Gauge", "\320\244\320\260\320\271\320\273", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Gauge: public Ui_Gauge {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAUGE_H
