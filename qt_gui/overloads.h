#ifndef OVERLOADS_H
#define OVERLOADS_H
#include "parameters.h"
#include <QtCore>

QDataStream &operator<<(QDataStream &out, const parameters &param);
QDataStream &operator>>(QDataStream &in, parameters &param);

QDataStream &operator<<(QDataStream &out, const parameters &param)
{
    out << param.origin[0] << param.origin[1] << param.origin[2];
    out << param.orientation[0] << param.orientation[1] << param.orientation[2];
    out << param.scale[0] << param.scale[1] << param.scale[2];// << param.orientation << param.scale;
    return out;
}

QDataStream &operator>>(QDataStream &in, parameters &param)
{
    param = parameters();
    in >> param.origin[0] >> param.origin[1] >> param.origin[2];
    in >> param.orientation[0] >> param.orientation[1] >> param.orientation[2];
    in >> param.scale[0] >> param.scale[1] >> param.scale[2];//  >> param.orientation >> param.scale;
    return in;
}


#endif // OVERLOADS_H
