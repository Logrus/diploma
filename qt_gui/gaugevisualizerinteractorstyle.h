#ifndef GAUGEVISUALIZERINTERACTORSTYLE_H
#define GAUGEVISUALIZERINTERACTORSTYLE_H
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/interactor_style.h>

enum InteractorKeyboardModifier
{
  INTERACTOR_KB_MOD_ALT,
  INTERACTOR_KB_MOD_CTRL,
  INTERACTOR_KB_MOD_SHIFT
};

namespace pcl {
namespace visualization{

class GaugeVisualizerInteractorStyle : public PCLVisualizerInteractorStyle
{
public:
   void OnKeyDown ();
   void OnChar ();
};

}
}


#endif // GAUGEVISUALIZERINTERACTORSTYLE_H
