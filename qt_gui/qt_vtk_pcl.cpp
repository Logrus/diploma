#include <QApplication>

#include "gauge.h"

int main(int argc, char** argv)
{
 QApplication app(argc, argv);
 Gauge w;
 if(argc==2)
 {
    w.openFile(argv[1]);
 }
 w.show();

 app.exec();

 return EXIT_SUCCESS;
}
