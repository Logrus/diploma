#include "specification.h"
#include <QTextDocument>
#include <QTextTable>
#include <QTextDocumentWriter>
#include <QTextCursor>
#include <QPrintDialog>
#include <QPrinter>
#include <QtGui>

// Порядковый номер
// Артикул
// Наименование по каталогу производителя
// Наименование по каталогу производителя на русском языке
// Единица измерения
// Количество

specification::specification()
    : m_document(new QTextDocument()),
      m_cursor(m_document),
      number(0)
{
    QTextCharFormat defaultFormat;
    defaultFormat.setFontPointSize(10);
    normalFormat = defaultFormat;
    boldFormat = defaultFormat;
    boldFormat.setFontWeight(QFont::Bold);
    QTextCharFormat titleFormat = boldFormat;
    titleFormat.setFontPointSize(14);

    centerFormat.setAlignment(Qt::AlignHCenter);

    m_cursor.setBlockFormat(centerFormat);
    m_cursor.insertText (QObject::trUtf8("Спецификация"), titleFormat);
    QTextTableFormat tableFormat;
    tableFormat.setCellPadding(5);
    tableFormat.setHeaderRowCount(1);
    tableFormat.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
    tableFormat.setWidth(QTextLength(QTextLength::PercentageLength, 100));
    m_cursor.insertTable(1,6,tableFormat);
    m_cursor.insertText(QObject::trUtf8("Порядковый номер"), boldFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(QObject::trUtf8("Артикул"), boldFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(QObject::trUtf8("Наименование по каталогу производителя"), boldFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(QObject::trUtf8("Наименование по каталогу производителя на русском языке"), boldFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(QObject::trUtf8("Единица измерения"), boldFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(QObject::trUtf8("Количество"), boldFormat);

}

specification::~specification()
{
    delete m_document;
}

void specification::write(const QString &fileName)
{
    QTextDocumentWriter writer(fileName);
    writer.write(m_document);
}

void specification::print(QPrinter &Printer)
{
        Printer.setFullPage(true);
        this->m_document->print(&Printer);
}

void specification::addObject(const QString &art,
                              const QString &name_catalog,
                              const QString &name_catalog_rus,
                              const QString &gauge,
                              int quantity)
{
    number++;
    QTextTable *table = m_cursor.currentTable();
    table->appendRows(1);
    m_cursor.movePosition(QTextCursor::PreviousRow);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(QString::number(number), normalFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(art, normalFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(name_catalog, normalFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(name_catalog_rus, normalFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(gauge, normalFormat);
    m_cursor.movePosition(QTextCursor::NextCell);
    m_cursor.insertText(QString::number(quantity), normalFormat);
}
