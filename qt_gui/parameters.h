#ifndef PARAMETERS_H
#define PARAMETERS_H
struct parameters
{
    double origin[3];
    double orientation[3];
    double scale[3];
};
#endif // PARAMETERS_H
