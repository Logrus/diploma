#ifndef GAUGE_H
#define GAUGE_H

#include <QMainWindow>
#include <QtGui>
#include <QtCore>

namespace Ui {
class Gauge;
}

class Gauge : public QMainWindow
{
    Q_OBJECT
    void addRoot(QString name);
    void addChild(QTreeWidgetItem *parent,QString name);
public:
    explicit Gauge(QWidget *parent = 0);
    void openFile(char *str);
    void loadPCD(const QString &fileName);
    void removeItem(const QString &itemName);

    ~Gauge();

private slots:
    void on_action_triggered();

    void on_action_2_triggered();

    void on_actionRegion_Growing_triggered();

    void on_hSliderMinCluster_actionTriggered(int action);

    void on_hSliderAngleRegionGrowing_actionTriggered(int action);

    void on_hSliderSetNeighbour_actionTriggered(int action);

    void on_pushButton_clicked();

    void on_action_4_triggered();

    void on_pushButton_2_clicked();

    void on_action_8_triggered();

    void on_pushButton_5_clicked();

    void on_listWidget_itemChanged(QListWidgetItem *item);

    void on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_hs_Registration_max_iterations_actionTriggered(int action);

    void on_hs_Registration_epsilon_actionTriggered(int action);

    void on_hs_Registration_squared_error_actionTriggered(int action);

    void on_hs_Registration_radius_actionTriggered(int action);

    void on_pushButton_Registration_start_clicked();

    void on_action_5_triggered();

    void on_action_3_triggered();

    void on_action_7_triggered();

    void on_action_6_triggered();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_button_DeleteAll_clicked();

private:
    Ui::Gauge *ui;
    QProgressBar *statProgressBar;
signals:
    //void fileLoadingStart(int val);
public slots:

};

#endif // GAUGE_H
