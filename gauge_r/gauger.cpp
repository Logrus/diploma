#include "gauger.h"
#include "build/ui_gauger.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <cmath>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <vtkRenderWindow.h>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/common/transforms.h>
#include <vtkPropPicker.h>
#include <pcl/registration/icp.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/project_inliers.h>
#include "kinfulsapp.h"

boost::shared_ptr<pcl::visualization::PCLVisualizer> pviz (new pcl::visualization::PCLVisualizer ("test_viz", false));

GaugeR::GaugeR(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GaugeR)
{
    ui->setupUi(this);
    vtkSmartPointer<vtkRenderWindow> renderWindow = pviz->getRenderWindow();
    ui->widget->SetRenderWindow (renderWindow);
    pviz->setupInteractor(ui->widget->GetInteractor(), ui->widget->GetRenderWindow());
    pviz->getInteractorStyle()->setKeyboardModifier(pcl::visualization::INTERACTOR_KB_MOD_SHIFT);
    pviz->setBackgroundColor(0, 0, 0);


    int device = 0;
    pcl::gpu::setDevice (device);
    pcl::gpu::printShortCudaDeviceInfo (device);

    std::string eval_folder, match_file, openni_device, oni_file, pcd_dir;
    try
    {
        capture.reset( new pcl::OpenNIGrabber() );
    }
    catch (const pcl::PCLException& /*e*/) { return cout << "Can't open depth source" << endl, -1; }

    float volume_size = pcl::device::kinfuLS::VOLUME_SIZE;

    float shift_distance = pcl::device::kinfuLS::DISTANCE_THRESHOLD;

    int snapshot_rate = pcl::device::kinfuLS::SNAPSHOT_RATE; // defined in device.h

    KinFuLSApp app (*capture, volume_size, shift_distance, snapshot_rate);


    if (pc::find_switch (argc, argv, "--registration") || pc::find_switch (argc, argv, "-r"))  {
        if (pcd_input) {
            app.pcd_source_   = true;
            app.registration_ = true; // since pcd provides registered rgbd
        } else {
            app.initRegistration();
        }
    }

    if (pc::find_switch (argc, argv, "--integrate-colors") || pc::find_switch (argc, argv, "-ic"))
        app.toggleColorIntegration();

    if (pc::find_switch (argc, argv, "--extract-textures") || pc::find_switch (argc, argv, "-et"))
        app.enable_texture_extraction_ = true;

    // executing
    if (triggered_capture)
        std::cout << "Capture mode: triggered\n";
    else
        std::cout << "Capture mode: stream\n";

    // set verbosity level
    pcl::console::setVerbosityLevel(pcl::console::L_VERBOSE);
    try { app.startMainLoop (triggered_capture); }
    catch (const pcl::PCLException& /*e*/) { cout << "PCLException" << endl; }
    catch (const std::bad_alloc& /*e*/) { cout << "Bad alloc" << endl; }
    catch (const std::exception& /*e*/) { cout << "Exception" << endl; }
    return 0;



    ui->widget->show();
}

GaugeR::~GaugeR()
{
    delete ui;
}
