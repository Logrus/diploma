#ifndef GAUGER_H
#define GAUGER_H

#include <QMainWindow>
#include <QtGui>
#include <QtCore>

namespace Ui {
class GaugeR;
}

class GaugeR : public QMainWindow
{
    Q_OBJECT

public:
    explicit GaugeR(QWidget *parent = 0);
    ~GaugeR();

private:
    Ui::GaugeR *ui;
};

#endif // GAUGER_H
